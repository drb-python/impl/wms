.. _install:

Installation of WMS driver
====================================
Installing ``drb-driver-wms`` with execute the following in a terminal:

.. code-block::

    pip install drb-driver-wms
