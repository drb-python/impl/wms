===================
Data Request Broker
===================
---------------------------------
Web Map Service driver for DRB
---------------------------------
Release v\ |version|.


.. image:: https://pepy.tech/badge/drb-driver-wms/month
    :target: https://pepy.tech/project/drb-driver-wms
    :alt: Requests Downloads Per Month Badge

.. image:: https://img.shields.io/pypi/l/drb-driver-wms.svg
    :target: https://pypi.org/project/drb-driver-wms/
    :alt: License Badge

.. image:: https://img.shields.io/pypi/wheel/drb-driver-wms.svg
    :target: https://pypi.org/project/drb-driver-wms/
    :alt: Wheel Support Badge

.. image:: https://img.shields.io/pypi/pyversions/drb-driver-wms.svg
    :target: https://pypi.org/project/drb-driver-wms/
    :alt: Python Version Support Badge

-------------------

This drb-driver-wms module driver access to Web Map Service (WMS).



User Guide
==========

.. toctree::
   :maxdepth: 2

   user/install


.. toctree::

   dev/api



