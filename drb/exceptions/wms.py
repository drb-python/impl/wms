from drb.exceptions.core import DrbException


class WmsRequestException(DrbException):
    pass
